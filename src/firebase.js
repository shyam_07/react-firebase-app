import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import { functions } from "firebase";


const firebaseConfig = {
    apiKey: "AIzaSyCsv_D3OzbvZl6rS7RP8ATpArP8zhrFz_0",
    authDomain: "userform-dde7e.firebaseapp.com",
    databaseURL: "https://userform-dde7e.firebaseio.com",
    projectId: "userform-dde7e",
    storageBucket: "userform-dde7e.appspot.com",
    messagingSenderId: "677818079733",
    appId: "1:677818079733:web:c1925f88386007b979e8bc",
    measurementId: "G-QXL89YY1DK"
};

//firebase initialize
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

//Google login
const provider = new firebase.auth.GoogleAuthProvider();
export const signInWithGoogle = () => {
    auth.signInWithPopup(provider);
};


//users doc generation, entry and get
export const generateUserDocument = async (user, additionalData) => {
    if (!user) return;
    //users document
    const userRef = firestore.doc(`users/${user.uid}`);
    const snapshot = await userRef.get();
    if (!snapshot.exists) {
      const { email, displayName, photoURL} = user;
      try {
        await userRef.set({
          displayName,
          email,
          photoURL,
          ...additionalData
        });
      } catch (error) {
        console.error("Error creating user document", error);
      }
    }
    return getUserDocument(user.uid);
};
const getUserDocument = async uid => {
    if (!uid) return null;
    try {
      const userDocument = await firestore.doc(`users/${uid}`).get();
      return {
        uid,
        ...userDocument.data()
      };
    } catch (error) {
      console.error("Error fetching user", error);
    }
};
